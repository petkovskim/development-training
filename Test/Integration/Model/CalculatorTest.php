<?php

namespace Rocket\DivingScore\Test\Integration\Model;

use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;
use Rocket\DivingScore\Api\Data\CompetitionRulesInterface;
use Rocket\DivingScore\Api\Data\CompetitionRulesInterfaceFactory;
use Rocket\DivingScore\Model\Calculator;

/**
 * Test the calculator model
 */
class CalculatorTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var CompetitionRulesInterface
     */
    private $competitionRules;

    /**
     * @var Calculator
     */
    private $targetModel;

    /**
     * {@inheritDoc}
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();
        $this->setCompetitionRules();
        $this->targetModel = $this->objectManager->create(Calculator::class);
    }

    /**
     * Set the competition rules
     */
    private function setCompetitionRules()
    {
        $this->competitionRules = $this->objectManager->create(CompetitionRulesInterfaceFactory::class)->create();

        $this->competitionRules->setNumberOfExtremeScoresToRemove(2);
    }

    /**
     * @param array $scores
     * @param float $difficulty
     * @param float $expectedResult
     * @dataProvider divingScoreProvider
     */
    public function testDivingScore(array $scores, float $difficulty, float $expectedResult)
    {
        $this->assertEquals(
            $expectedResult,
            $this->targetModel->calculate(
                $this->competitionRules,
                $scores,
                $difficulty
            )
        );
    }

    /**
     * @return array
     */
    public function divingScoreProvider(): array
    {
        return [
            '#1' => [
                [1,2,3,3,3,4,5],
                1,
                9,
            ],
            '#2' => [
                [1,2,3,3,3,4,5],
                1.1,
                9.9,
            ],
            '#3' => [
                [5,1,2,3,3,4,3],
                1.1,
                9.9,
            ],
            '#4' => [
                [5,1,2,3.05,3.05,4,3.05],
                1.15,
                10.52,
            ],
            '#5' => [
                [5,1,2,3.05,3.05,4,3.05],
                0,
                0,
            ],
            '#6' => [
                [0,0,0,0,0,0,0],
                1,
                0,
            ],
        ];
    }
}
