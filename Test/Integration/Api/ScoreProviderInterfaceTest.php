<?php

namespace Rocket\DivingScore\Test\Integration\Api;

use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;
use Rocket\DivingScore\Api\Data\CompetitionRulesInterface;
use Rocket\DivingScore\Api\Data\CompetitionRulesInterfaceFactory;
use Rocket\DivingScore\Api\ScoreProviderInterface;
use Rocket\DivingScore\Model\Exception\InputDifficultyNumberException;
use Rocket\DivingScore\Model\Exception\InputScoresException;

/**
 * Test Score Provider Interface
 */
class ScoreProviderInterfaceTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var CompetitionRulesInterface
     */
    private $competitionRules;

    /**
     * @var ScoreProviderInterface
     */
    private $targetModel;

    /**
     * {@inheritDoc}
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();
        $this->setCompetitionRules();
        $this->targetModel = $this->objectManager->create(ScoreProviderInterface::class);
    }

    /**
     * Set the competition rules
     */
    private function setCompetitionRules()
    {
        $this->competitionRules = $this->objectManager->create(CompetitionRulesInterfaceFactory::class)->create();

        $this->competitionRules
            ->setNumberOfJudges(7)
            ->setNumberOfExtremeScoresToRemove(2);
    }

    /**
     * Test Amount Of Scores Less Than Expected
     */
    public function testAmountOfScoresLessThanExpected()
    {
        $this->expectException(InputScoresException::class);
        $this->expectExceptionMessageMatches('/Invalid amount of scores. Expected 7, received 6/');

        $this->targetModel->getDivingScore(
            $this->competitionRules,
            [1,2,3,4,5,6],
            1.1
        );
    }

    /**
     * Test Amount Of Scores More Than Expected
     */
    public function testAmountOfScoresMoreThanExpected()
    {
        $this->expectException(InputScoresException::class);
        $this->expectExceptionMessageMatches('/Invalid amount of scores. Expected 7, received 8/');

        $this->targetModel->getDivingScore(
            $this->competitionRules,
            [1,2,3,4,5,6,7,8],
            1.1
        );
    }

    /**
     * Test Score Below Minimum value
     */
    public function testScoreBelowMinimumValue()
    {
        $this->expectException(InputScoresException::class);
        $this->expectExceptionMessageMatches('/Score value 0.5 is invalid/');

        $this->targetModel->getDivingScore(
            $this->competitionRules,
            [1,2,3,0.5,5,6,7],
            1.1
        );
    }

    /**
     * Test Score Above Maximum value
     */
    public function testScoreAboveMaximumValue()
    {
        $this->expectException(InputScoresException::class);
        $this->expectExceptionMessageMatches('/Score value 10.7 is invalid/');

        $this->targetModel->getDivingScore(
            $this->competitionRules,
            [1,2,3,1.5,5,6,10.7],
            1.1
        );
    }

    /**
     * Test Score Is Not Float
     */
    public function testScoreIsNotFloat()
    {
        $this->expectException(InputScoresException::class);
        $this->expectExceptionMessageMatches('/Score value invalid-string is invalid/');

        $this->targetModel->getDivingScore(
            $this->competitionRules,
            [1,2,3,'invalid-string',5,6,10.7],
            1.1
        );
    }

    public function testInvalidDifficultyNumber()
    {
        $this->expectException(InputDifficultyNumberException::class);
        $this->expectExceptionMessageMatches('/Difficulty level value -1 is invalid/');


        $this->targetModel->getDivingScore(
            $this->competitionRules,
            [1,2,3,4,5,6,7],
            -1
        );
    }

    /**
     * @param array $scores
     * @param float $difficulty
     * @param float $expectedResult
     *
     * @dataProvider divingScoreProvider
     */
    public function testGetDivingScore(array $scores, float $difficulty, float $expectedResult)
    {
        $this->assertEquals(
            $expectedResult,
            $this->targetModel->getDivingScore(
                $this->competitionRules,
                $scores,
                $difficulty
            )
        );
    }

    /**
     * @return array
     */
    public function divingScoreProvider(): array
    {
        return [
            '#1' => [
                [1,2,3,3,3,4,5],
                1,
                9,
            ],
            '#2' => [
                [1,2,3,3,3,4,5],
                1.1,
                9.9,
            ],
            '#3' => [
                [5,1,2,3,3,4,3],
                1.1,
                9.9,
            ],
            '#4' => [
                [5,1,2,3.05,3.05,4,3.05],
                1.15,
                10.52,
            ],
        ];
    }
}
