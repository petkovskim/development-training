<?php

namespace Rocket\DivingScore\Test\Unit;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Rocket\DivingScore\Model\CompetitionRules;
use Rocket\DivingScore\Model\Calculator;
use Rocket\DivingScore\Model\DivingScore\RemoveComposite;

/**
 * Test the DivingScore
 */
class CalculatorTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var CompetitionRules
     */
    private $competitionRules;

    /**
     * @var RemoveComposite | MockObject
     */
    private $removeComposite;

    /**
     * @var Calculator
     */
    private $targetModel;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->objectManager = new ObjectManager($this);

        $this->populateCompetitionRules();

        $this->removeComposite = $this->getMockBuilder(RemoveComposite::class)
            ->disableOriginalConstructor()
            ->setMethods(['remove'])
            ->getMock();

        $this->targetModel = $this->objectManager->getObject(Calculator::class, [
            'removeComposite' => $this->removeComposite
        ]);
    }

    /**
     * Populate Competition Rules
     */
    private function populateCompetitionRules()
    {
        $this->competitionRules = $this->objectManager->getObject(CompetitionRules::class);

        $this->competitionRules->setNumberOfExtremeScoresToRemove(2);
    }

    /**
     * @param array $scores
     * @param array $scoresCleaned
     * @param float $difficulty
     * @param float $expectedResult
     *
     * @dataProvider providerCalculation
     */
    public function testCalculation(array $scores, array $scoresCleaned, float $difficulty, float $expectedResult)
    {
        $this->removeComposite->expects($this->once())
            ->method('remove')
            ->with($scores, $this->competitionRules->getNumberOfExtremeScoresToRemove())
            ->willReturn($scoresCleaned);

        $this->assertEquals(
            $expectedResult,
            $this->targetModel->calculate(
                $this->competitionRules,
                $scores,
                $difficulty
            )
        );
    }

    /**
     * @return array
     */
    public function providerCalculation(): array
    {
        return [
            '#1' => [
                [1,2,3,3,3,4,5],
                [3,3,3],
                1,
                9,
            ],
            '#2' => [
                [1,2,3,3,3,4,5],
                [3,3,3],
                1.1,
                9.9,
            ],
            '#3' => [
                [5,1,2,3,3,4,3],
                [3,3,3],
                1.1,
                9.9,
            ],
            '#4' => [
                [5,1,2,3.05,3.05,4,3.05],
                [3.05,3.05,3.05],
                1.15,
                10.52,
            ],
            '#5' => [
                [5,1,2,3.05,3.05,4,3.05],
                [3.05,3.05,3.05],
                0,
                0,
            ],
            '#6' => [
                [0,0,0,0,0,0,0],
                [0,0,0],
                1,
                0,
            ],
            '#7' => [
                [0,0,3,-3,5,6,6],
                [3,-3,5],
                1,
                5,
            ],
        ];
    }
}
