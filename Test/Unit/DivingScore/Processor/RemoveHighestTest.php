<?php

namespace Rocket\DivingScore\Test\Unit\DivingScore\Processor;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Rocket\DivingScore\Model\DivingScore\Processor\RemoveHighest;

/**
 * Test Processor RemoveHighest
 */
class RemoveHighestTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var RemoveHighest
     */
    private $targetModel;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->objectManager = new ObjectManager($this);
        $this->targetModel = $this->objectManager->getObject(RemoveHighest::class);
    }

    /**
     * Test Remove Highest
     *
     * @param array $scores
     * @param int $count
     * @param array $expectedResult
     *
     * @dataProvider providerRemoveHighest
     */
    public function testRemoveLowest(array $scores, int $count, array $expectedResult)
    {
        $this->assertEquals($expectedResult, $this->targetModel->remove($scores, $count));
    }

    /**
     * @return array
     */
    public function providerRemoveHighest(): array
    {
        return [
            '#1' => [
                [1,2,3,4,5,6],
                2,
                [1,2,3,4]
            ],
            '#2' => [
                [6,4,3,4,5,6],
                2,
                [3,4,4,5]
            ],
            '#3' => [
                [0,0,0,-1,-3],
                2,
                [-3,-1,0]
            ],
            '#4' => [
                [1,2,3,4,5],
                -2,
                [1,2,3,4,5]
            ],
            '#5' => [
                [1,2,3,4,5],
                0,
                [1,2,3,4,5]
            ],
        ];
    }
}
