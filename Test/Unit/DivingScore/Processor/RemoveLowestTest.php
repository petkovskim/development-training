<?php

namespace Rocket\DivingScore\Test\Unit\DivingScore\Processor;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\TestCase;
use Rocket\DivingScore\Model\DivingScore\Processor\RemoveLowest;

/**
 * Test Processor RemoveLowest
 */
class RemoveLowestTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var RemoveLowest
     */
    private $targetModel;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->objectManager = new ObjectManager($this);
        $this->targetModel = $this->objectManager->getObject(RemoveLowest::class);
    }

    /**
     * Test Remove Lowest
     *
     * @param array $scores
     * @param int $count
     * @param array $expectedResult
     *
     * @dataProvider providerRemoveLowest
     */
    public function testRemoveLowest(array $scores, int $count, array $expectedResult)
    {
        $this->assertEquals($expectedResult, $this->targetModel->remove($scores, $count));
    }

    /**
     * @return array
     */
    public function providerRemoveLowest(): array
    {
        return [
            '#1' => [
                [1,2,3,4,5,6],
                2,
                [3,4,5,6]
            ],
            '#2' => [
                [6,4,3,4,5,6],
                2,
                [4,5,6,6]
            ],
        ];
    }
}
