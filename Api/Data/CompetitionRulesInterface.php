<?php

namespace Rocket\DivingScore\Api\Data;

/**
 * Interface Competition Rules
 */
interface CompetitionRulesInterface
{
    /** @var string  */
    const NUMBER_OF_JUDGES = 'number_of_judges';

    /** @var string  */
    const NUMBER_OF_EXTREME_SCORES_TO_REMOVE = 'number_of_extreme_scores_to_remove';

    /**
     * @return int
     */
    public function getNumberOfJudges(): int;

    /**
     * @return int
     */
    public function getNumberOfExtremeScoresToRemove(): int;

    /**
     * @param int $numberOfJudges
     * @return $this
     */
    public function setNumberOfJudges(int $numberOfJudges): self;

    /**
     * @param int $numberOfExtremeScoresToRemove
     * @return $this
     */
    public function setNumberOfExtremeScoresToRemove(int $numberOfExtremeScoresToRemove): self;
}
