<?php

namespace Rocket\DivingScore\Api;

use Rocket\DivingScore\Api\Data\CompetitionRulesInterface;
use Rocket\DivingScore\Model\Exception\InputDifficultyNumberException;
use Rocket\DivingScore\Model\Exception\InputScoresException;

/**
 * Interface Score Provider
 */
interface ScoreProviderInterface
{
    /**
     * Get the final diving score based on competition rules, individual scores and difficulty level
     *
     * @param CompetitionRulesInterface $competitionRules
     * @param array $individualScores
     * @param float $difficultyLevel
     * @return float
     * @throws InputScoresException
     * @throws InputDifficultyNumberException
     */
    public function getDivingScore(
        CompetitionRulesInterface $competitionRules,
        array $individualScores,
        float $difficultyLevel
    ): float;
}
