<?php

namespace Rocket\DivingScore\Api\Metadata;

/**
 * Interface Calculation
 */
interface CalculationInterface
{
    /** @var int  */
    const MAX_INDIVIDUAL_SCORE = 10;

    /** @var int  */
    const MIN_INDIVIDUAL_SCORE = 1;

    /** @var int  */
    const FINAL_SCORE_PRECISION = 2;

    const MIN_DIFFICULTY_LEVEL = 1;

    const MAX_DIFFICULTY_LEVEL = 5;
}
