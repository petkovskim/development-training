<?php

namespace Rocket\DivingScore\Model;

use Rocket\DivingScore\Api\Data\CompetitionRulesInterface;
use Rocket\DivingScore\Api\ScoreProviderInterface;
use Rocket\DivingScore\Model\Validator\DifficultyLevelValidator;
use Rocket\DivingScore\Model\Validator\ScoreValidator;

/**
 * Implementation of Score Provider Interface
 */
class ScoreProvider implements ScoreProviderInterface
{
    /**
     * @var DifficultyLevelValidator
     */
    private $difficultyLevelValidator;

    /**
     * @var ScoreValidator
     */
    private $scoreValidator;

    /**
     * @var Calculator
     */
    private $calculator;


    /**
     * @param ScoreValidator $scoreValidator
     * @param Calculator $calculator
     */
    public function __construct(
        DifficultyLevelValidator $difficultyLevelValidator,
        ScoreValidator $scoreValidator,
        Calculator $calculator
    ) {
        $this->difficultyLevelValidator = $difficultyLevelValidator;
        $this->scoreValidator = $scoreValidator;
        $this->calculator = $calculator;
    }

    /**
     * {@inheritDoc}
     */
    public function getDivingScore(
        CompetitionRulesInterface $competitionRules,
        array $individualScores,
        float $difficultyLevel
    ): float {
        $this->difficultyLevelValidator->validate($difficultyLevel);
        $this->scoreValidator->validate($competitionRules, $individualScores);

        return $this->calculator->calculate($competitionRules, $individualScores, $difficultyLevel);
    }
}
