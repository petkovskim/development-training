<?php

namespace Rocket\DivingScore\Model\Exception;

use Magento\Framework\Exception\LocalizedException;

class InputDifficultyNumberException extends LocalizedException
{
}
