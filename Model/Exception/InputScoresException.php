<?php

namespace Rocket\DivingScore\Model\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Exception raised when invalid Input Scores are encountered
 */
class InputScoresException extends LocalizedException
{
}
