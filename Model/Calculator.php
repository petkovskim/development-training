<?php

namespace Rocket\DivingScore\Model;

use Rocket\DivingScore\Api\Data\CompetitionRulesInterface;
use Rocket\DivingScore\Api\Metadata\CalculationInterface;
use Rocket\DivingScore\Model\DivingScore\RemoveInterface;

/**
 * Input results from K judges
 * L lowest and L highest are removed
 * Remaining M are summed and multiplied by the difficulty level
 */
class Calculator
{
    /**
     * @var RemoveInterface
     */
    private $removeComposite;

    /**
     * @param RemoveInterface $removeComposite
     */
    public function __construct(
        RemoveInterface $removeComposite
    ) {
        $this->removeComposite = $removeComposite;
    }

    /**
     * @param CompetitionRulesInterface $competitionRules
     * @param array $scores
     * @param float $difficultyLevel
     * @return float
     */
    public function calculate(CompetitionRulesInterface $competitionRules, array $scores, float $difficultyLevel): float
    {
        $scores = $this->removeComposite->remove(
            $scores,
            $competitionRules->getNumberOfExtremeScoresToRemove()
        );

        return round(array_sum($scores) * $difficultyLevel, CalculationInterface::FINAL_SCORE_PRECISION);
    }
}
