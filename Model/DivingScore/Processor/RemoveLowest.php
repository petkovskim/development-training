<?php

namespace Rocket\DivingScore\Model\DivingScore\Processor;

use Rocket\DivingScore\Model\DivingScore\RemoveInterface;

/**
 * Processor Remove Lowest
 */
class RemoveLowest implements RemoveInterface
{
    /**
     * @inheritDoc
     */
    public function remove(array $scores, int $count): array
    {
        sort($scores);
        $scores = array_slice($scores, $count);

        return $scores;
    }
}
