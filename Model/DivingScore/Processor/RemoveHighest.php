<?php

namespace Rocket\DivingScore\Model\DivingScore\Processor;

use Rocket\DivingScore\Model\DivingScore\RemoveInterface;

/**
 * Processor Remove Highest
 */
class RemoveHighest implements RemoveInterface
{
    /**
     * @inheritDoc
     */
    public function remove(array $scores, int $count): array
    {
        if ($count < 1) {
            return $scores;
        }

        sort($scores);
        $scores = array_splice($scores, 0, -$count);

        return $scores;
    }
}
