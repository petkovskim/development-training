<?php

namespace Rocket\DivingScore\Model\DivingScore;

/**
 * Interface Remove
 */
interface RemoveInterface
{
    /**
     * @param array $scores
     * @param int $count
     * @return array
     */
    public function remove(array $scores, int $count): array;
}
