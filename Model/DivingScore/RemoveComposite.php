<?php

namespace Rocket\DivingScore\Model\DivingScore;

/**
 * Composite Processor
 */
class RemoveComposite implements RemoveInterface
{
    /**
     * @var RemoveInterface[]
     */
    private $processors;

    /**
     * @param array $processors
     */
    public function __construct(
        array $processors
    ) {
        foreach ($processors as $processor) {
            if ($processor instanceof RemoveInterface) {
                $this->processors[] = $processor;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function remove(array $scores, int $count): array
    {
        foreach ($this->processors as $processor) {
            $scores = $processor->remove($scores, $count);
        }

        return $scores;
    }
}
