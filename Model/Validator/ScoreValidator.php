<?php

namespace Rocket\DivingScore\Model\Validator;

use Rocket\DivingScore\Api\Data\CompetitionRulesInterface;
use Rocket\DivingScore\Api\Metadata\CalculationInterface;
use Rocket\DivingScore\Model\Exception\InputScoresException;

/**
 * Validator Score
 */
class ScoreValidator
{
    /**
     * @param CompetitionRulesInterface $competitionRules
     * @param array $scores
     * @throws InputScoresException
     */
    public function validate(CompetitionRulesInterface $competitionRules, array $scores)
    {
        if (count($scores) != $competitionRules->getNumberOfJudges()) {
            throw new InputScoresException(
                __(
                    'Invalid amount of scores. Expected %1, received %2',
                    $competitionRules->getNumberOfJudges(),
                    count($scores)
                )
            );
        }

        foreach ($scores as $score) {
            if (!filter_var($score, FILTER_VALIDATE_FLOAT)
                || $score < CalculationInterface::MIN_INDIVIDUAL_SCORE
                || $score > CalculationInterface::MAX_INDIVIDUAL_SCORE
            ) {
                throw new InputScoresException(
                    __(
                        'Score value %1 is invalid',
                        $score
                    )
                );
            }
        }
    }
}
