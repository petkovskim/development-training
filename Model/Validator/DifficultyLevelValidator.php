<?php

namespace Rocket\DivingScore\Model\Validator;

use Rocket\DivingScore\Api\Metadata\CalculationInterface;
use Rocket\DivingScore\Model\Exception\InputDifficultyNumberException;

/**
 * Validator Difficulty
 */
class DifficultyLevelValidator
{
    /**
     * @param float $difficultyLevel
     * @throws InputDifficultyNumberException
     */
    public function validate(float $difficultyLevel)
    {
        if (!filter_var($difficultyLevel, FILTER_VALIDATE_FLOAT)) {
            throw new InputDifficultyNumberException(
                __(
                    'Invalid difficulty level'
                )
            );
        }

        if ($difficultyLevel < CalculationInterface::MIN_DIFFICULTY_LEVEL
            || $difficultyLevel >= CalculationInterface::MAX_DIFFICULTY_LEVEL
        ) {
            throw new InputDifficultyNumberException(
                __(
                    'Difficulty level value %1 is invalid',
                    $difficultyLevel
                )
            );
        }
    }
}
