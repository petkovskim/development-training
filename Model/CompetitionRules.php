<?php

namespace Rocket\DivingScore\Model;

use Magento\Framework\Model\AbstractModel;
use Rocket\DivingScore\Api\Data\CompetitionRulesInterface;

/**
 * Default implementation of CompetitionRulesInterface
 */
class CompetitionRules extends AbstractModel implements CompetitionRulesInterface
{
    /**
     * @inheritDoc
     */
    public function getNumberOfJudges(): int
    {
        return $this->getData(self::NUMBER_OF_JUDGES);
    }

    /**
     * @inheritDoc
     */
    public function getNumberOfExtremeScoresToRemove(): int
    {
        return $this->getData(self::NUMBER_OF_EXTREME_SCORES_TO_REMOVE);
    }

    /**
     * @inheritDoc
     */
    public function setNumberOfJudges(int $numberOfJudges): CompetitionRulesInterface
    {
        return $this->setData(self::NUMBER_OF_JUDGES, $numberOfJudges);
    }

    /**
     * @inheritDoc
     */
    public function setNumberOfExtremeScoresToRemove(int $numberOfExtremeScoresToRemove): CompetitionRulesInterface
    {
        return $this->setData(self::NUMBER_OF_EXTREME_SCORES_TO_REMOVE, $numberOfExtremeScoresToRemove);
    }
}
